﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic.LinkedList<T>;

public class Character
{
    #region Character Stats

    public string characterName; //name entered by player
    public string characterType; //what character type is the player
    public int characterStr; //Strength: base damage done
    public int characterAgl; //Agility: increases priority in battle and crit rate
    public int characterInt; //Inteligence: perhaps used for dialog options ???
    public int characterMag; //Magic: determines what what spells can be used as well as strength of offensive/defensive spells
    public int characterManaM;//base mana stat
    public int characterManaC;//actual mana
    public int characterHPM; //base HP stat
    public int characterHPC; //actual health
    public int characterXP; //Experience: determines level.
    public int characterLvl; //Level: level of the character
    public string Discription; //character discription
    public string gender; //character gender
    public int positionX; //map postion of X coordinate
    public int postiionY; //map postion of Y coordinate
    public int positionZ; //map postion of Z coordinate
    public int expDrop; //experience dropped apon kill for monsters only
    public string elementalWeak; //elemental weakness take 25% more damage
    public string elementalStr; //elemental strength take 25% less damage
    public Equips helmet; //helmet the character is wearing
    public Equips torso; //shirt like armor the character is wearing
    public Equips hands; //gloves
    public Equips legs; //pants
    public Equips feet; //shoes
    public Equips weapon; //swords, staffs, axes, bare
    public Equips offhand1; //like a ring or an amulet 

    #endregion
    public Stuff levelUp = new Stuff();//dictionary for level up NEEDS WORKED ON
    public Character(string name, string type, string gen)
	{
        

        characterName = name;
        characterType = type;
        gender = gen;

        if (type.ToLower() == "swordsman")
        {
            characterStr = 10;
            characterAgl = 5;
            characterInt = 2;
            characterMag = 0;
            characterHP = 15;
            characterXP = 0;
        }
        else if (type.ToLower() == "archer")
        {
            characterStr = 5;
            characterAgl = 10;
            characterInt = 3;
            characterMag = 0;
            characterHP = 10;
            characterXP = 0;
        }
        else if (type.ToLower() == "mage")
        {
            characterStr = 5;
            characterAgl = 5;
            characterInt = 5;
            characterMag = 15;
            characterHP = 10;
            characterXP = 0;
        }
        else if (type.ToLower() == "barbarian")
        {
            characterStr = 15;
            characterAgl = 7;
            characterInt = 0;
            characterMag = 0;
            characterHP = 20;
            characterXP = 0;

        }
        else if (type.ToLower() == "debug")
        {
            characterStr = 50;
            characterAgl = 50;
            characterInt = 50;
            characterMag = 150;
            characterHP = 100;
            characterXP = 0;
        }
	}

    #region Character Functions

    //public character levelup(character c)
    //{


    //    if (c.characterxp > levelup[c.characterlvl])
    //    {
    //        ++c.characterlvl;
    //    }


    //}

    public Character Teleport(Character c, int x, int y, int z)
    {
        //not sure if i actually want this to be a spell so for now it's here for potential debug purpases
        //when mape is implemented make sure to allow such a feature to work.

        if (c.characterType == "debug")
        {
            c.positionX = x;
            c.postiionY = y;
            c.positionZ = z;
        }
        else
        {
            Console.WriteLine("you don't appear to be able to do that");
        }
    }
    #endregion
}
