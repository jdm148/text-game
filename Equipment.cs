﻿using System;

public class Equipment
{
    public Equips defaultEquip = new Equips();// bare hands
    public Equips standSword = new Equips();//starting weapon of swordsman
    public Equips standAxe = new Equips();//starting weapon of barbrian
    public Equips standBow = new Equips();//starting weapon of archer
    public Equips standStaff = new Equips();//starting weapon of mage
    public Equips bestSword = new Equipds();//best sword in game, used for debug character hidden for rest



    public Equipment()
	{
	}

    standSword.type = "sword";
    standSword.name = "Standard Sword";
    standSword.description = "A standard sword, used for slashing away foes";
    standSword.strengthChange = 5;
    
    standAxe.type = "axe";
    standAxe.name = "Standard Axe";
    standAxe.description = "A standard axe, equaling good at chopping trees and enemies";
    standAxe.strengthChange = 5;

    standBow.type = "bow";
    standBow.name = "Standard Bow";
    standBow.description = "A standard bow, best used at a distance but don't let me tell you how to live your life";
    standBow.agilityChange = 5;

    standStaff.type = "staff";
    standStaff.name = "Standard Staff";
    standStaff.description = "To others this may just be a stick, but to you it's a way to channel your magic";
    standStaff.magicChange = 5;
    standStaff.maxManaChange = 15;

    bestSword.type = "sword"
    bestSword.name = "Laser Sword"
    bestSword.description = "Because all the cool names were taken";
    bestSword.maxHealthChange = 50;
    bestSword.strengthChange = 15;
    bestSword.agilityChange = 15;
    bestSword.intelChange = 25;
    bestSword.magicChange = 15;
    bestSword.maxManaChange = 50;
    bestSword.defChange = 50;




}
