﻿using System;

public class Equips
{
    public string type;
    public string name;
    public string description;
    public int maxHealthChange;
    public int agilityChange;
    public int strengthChange;
    public int intelChange;
    public int magicChange;
    public int maxManaChange;
    public int defChange;
    public string elementW; //element weakness
    public string elementS; //element strength 


	public Equips()
	{
        type = "default";
        name = "default";
        description = "default";
        maxHealthChange = 0;
        agilityChange = 0;
        strengthChange = 0;
        intelChange = 0;
        magicChange = 0;
        maxManaChange = 0;
        defChange = 0;
        elementS = "default";
        elementW = "default";

    }





}
