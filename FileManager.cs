﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

public class FileManager
{
	public FileManager()
	{
        string folderName = @"c:\TextGame";
        string pathString = System.IO.Path.Combine(folderName, "TextGameSaveFile");
        System.IO.Directory.CreateDirectory(pathString);
        string fileName = "SaveFile.txt";
        pathString = System.IO.Path.Combine(pathString, fileName);
        Console.WriteLine("Path to my file: {0}\n", pathString);

    }

    #region SaveLoad

    public void SaveFile(Character c)
    {
        using (StreamWriter writer = File.AppendText(@"c:\TextGame\TextGameSaveFile\SaveFile.txt"))
        {
            //be sure to add adition stat changes as we make them to the Character file
            writer.Write(c.characterName + Environment.NewLine);
            writer.Write(c.characterType + Environment.NewLine);
            writer.Write(c.gender + Environment.NewLine);
            writer.Write(c.characterStr + Environment.NewLine);
            writer.Write(c.characterSta + Environment.NewLine);
            writer.Write(c.characterInt + Environment.NewLine);
            writer.Write(c.characterMag + Environment.NewLine);
            writer.Write(c.characterHP + Environment.NewLine);
            writer.Write(c.characterXP + Environment.NewLine);
            writer.Write(c.characterLvl + Environment.NewLine);
            writer.Write(c.positionX + Environment.NewLine);
            writer.Write(c.positionY + Environment.NewLine);
            writer.Write(c.positionZ + Environment.NewLine);





        }

    }

    public void ReadFile(Character c)
    {
        //be sure to add adition stat changes as we make them to the Character file
        System.IO.StreamReader file = new System.IO.StreamReader(@"c:\TextGame\TextGameSaveFile\SaveFile.txt");
        c.characterName = file.ReadLine();
        c.characterType = file.ReadLine();
        c.gender = file.ReadLine();
        c.characterStr = Convert.ToInt32(file.ReadLine());
        c.characterSta = Convert.ToInt32(file.ReadLine());
        c.characterInt = Convert.ToInt32(file.ReadLine());
        c.characterMag = Convert.ToInt32(file.ReadLine());
        c.characterHP = Convert.ToInt32(file.ReadLine());
        c.characterXP = Convert.ToInt32(file.ReadLine());
        c.characterLvl = Convert.ToInt32(file.ReadLine());
        c.positionX = Convert.ToInt32(file.ReadLine());
        c.positionY = Convert.ToInt32(file.ReadLine());
        c.positionZ = Convert.ToInt32(file.ReadLine());

        file.Close();



    }
    #endregion
}
