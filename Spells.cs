﻿using System;

public class Spells
{

	public Spells()
	{
	}

    #region Spell Functions

    #region Healling
    public void HealOne(Character c) //heals 25% of max health
    {
        if (c.characterManaC >= 15)
        {
            c.characterHPC = c.characterHPC + (c.characterHPM * .25);
            if (c.characterHPC > c.characterHPM)
            {
                c.characterHPC = c.characterHPM;
            }

        }
        else
        {
            Console.WriteLine("Not Enough Mana");
        }
    }

    public void HealTwo(Character c) //heals 50% of max health
    {
        if (c.characterManaC >= 25)
        {
            c.characterHPC = c.characterHPC + (c.characterHPM * .50);
            if (c.characterHPC > c.characterHPM)
            {
                c.characterHPC = c.characterHPM;
            }

        }
        else
        {
            Console.WriteLine("Not Enough Mana");
        }
    }

    public void HealMax(Character c) //heals 100% of max health
    {
        if (c.characterManaC >= 35)
        {
            c.characterHPC = c.characterHPM;
        }
        else
        {
            Console.WriteLine("Not Enough Mana");
        }
    }
    #endregion

    #region Offense

    public int FireOne(Character c, Monster m) //called Fire Ball on UI
    {
        //returned int should be damage delt to monster, should not be allowed to be called in field unless we implement some sort of obsticle for it.
        //damage should probably be calculated randomly but based of character magic level and monster magic resist level
        //magic resist currently needs implement so delete this line when it is.
        //if weak agianst fire does double damage
    }

    public int FireTwo(Character c, Monster m) //called Magma Shot on UI
    {
        //returned int should be damage delt to monster, should not be allowed to be called in field unless we implement some sort of obsticle for it.
        //damage should probably be calculated randomly but based of character magic level and monster magic resist level
        //magic resist currently needs implement so delete this line when it is.
        //also perhaps specific monsters should be weak to specific elements
        //if weak agianst fire does double damage
    }

    public int FireThree(Character c, Monster m) //called Solar Flare on UI
    {
        //returned int should be damage delt to monster, should not be allowed to be called in field unless we implement some sort of obsticle for it.
        //damage should probably be calculated randomly but based of character magic level and monster magic resist level
        //magic resist currently needs implement so delete this line when it is.
        //also perhaps specific monsters should be weak to specific elements
        //if weak agianst fire does double damage
    }

    public int LightningOne(Character c, Monster m) //called Lightning Strike on UI
    {
        //returned int should be damage delt to monster, should not be allowed to be called in field unless we implement some sort of obsticle for it.
        //damage should probably be calculated randomly but based of character magic level and monster magic resist level
        //magic resist currently needs implement so delete this line when it is.
        //also perhaps specific monsters should be weak to specific elements
        //if weak agianst lightning does double damage
    }

    public int LightningTwo(Character c, Monster m) //called Thunder Bolr on UI
    {
        //returned int should be damage delt to monster, should not be allowed to be called in field unless we implement some sort of obsticle for it.
        //damage should probably be calculated randomly but based of character magic level and monster magic resist level
        //magic resist currently needs implement so delete this line when it is.
        //also perhaps specific monsters should be weak to specific elements
        //if weak agianst lightning does double damage
    }

    public int LightningThree(Character c, Monster m) //called Plasma Ball on UI
    {
        //returned int should be damage delt to monster, should not be allowed to be called in field unless we implement some sort of obsticle for it.
        //damage should probably be calculated randomly but based of character magic level and monster magic resist level
        //magic resist currently needs implement so delete this line when it is.
        //also perhaps specific monsters should be weak to specific elements
        //if weak agianst lightning does double damage
    }

    public int IceOne(Character c, Monster m) //called Ice Spike on UI
    {
        //returned int should be damage delt to monster, should not be allowed to be called in field unless we implement some sort of obsticle for it.
        //damage should probably be calculated randomly but based of character magic level and monster magic resist level
        //magic resist currently needs implement so delete this line when it is.
        //also perhaps specific monsters should be weak to specific elements
        //if weak agianst ice does double damage
    }

    public int IceTwo(Character c, Monster m) //called Glacial Crush on UI
    {
        //returned int should be damage delt to monster, should not be allowed to be called in field unless we implement some sort of obsticle for it.
        //damage should probably be calculated randomly but based of character magic level and monster magic resist level
        //magic resist currently needs implement so delete this line when it is.
        //also perhaps specific monsters should be weak to specific elements
        //if weak agianst ice does double damage
    }

    public int IceThree(Character c, Monster m) //called Absolute Zero on UI
    {
        //returned int should be damage delt to monster, should not be allowed to be called in field unless we implement some sort of obsticle for it.
        //damage should probably be calculated randomly but based of character magic level and monster magic resist level
        //magic resist currently needs implement so delete this line when it is.
        //also perhaps specific monsters should be weak to specific elements
        //if weak agianst ice does double damage
    }

    public int ChargeAtk(Character c, Monster m)
    {
        //returned int should be damage delt to monster, should not be allowed to be called in field.
        //damage should probably be calculated randomly but based of character atk level and monster defense level
        //damage should be at least 1.5x regular damage at the cost of taking two turns
        //this may need to be implemented directly into the battle system and not a Spell.
    }

    #endregion

    #region Defense
    public void DefUpOne(Character c)
    {
        //should only be called in battle
        //mana check
        //increase def stat a little
    }

    public void DefUpTwo(Character c)
    {
        //should only be called in battle
        //mana check
        //increase def stat a little more
    }

    public void DefUpThree(Character c)
    {
        //should only be called in battle
        //mana check
        //increase def stat a lot more
    }

    public void StrUpOne(Character c)
    {
        //should only be called in battle
        //mana check
        //increase str stat a little
    }

    public void StrUpTwo(Character c)
    {
        //should only be called in battle
        //mana check
        //increase str stat a little more
    }

    public void StrUpThree(Character c)
    {
        //should only be called in battle
        //mana check
        //increase str stat a lot more
    }

    public void FireSheild(Character c)
    {
        //changes adds a 25% damage reduction to fire element attacks in battle
        //goes away after battle
        //stacks with/cancels out base strength/resistance
        //meaning if you're strong against fire you take 50% damage and if weak agains fire you take normal damage
    }

    public void ThunderSheild(Character c)
    {
        //changes adds a 25% damage reduction to thunder element attacks in battle
        //goes away after battle
        //stacks with/cancels out base strength/resistance
        //meaning if you're strong against Thunder you take 50% damage and if weak agains Thunder you take normal damage
    }

    public void IceSheild(Character c)
    {
        //changes adds a 25% damage reduction to ice element attacks in battle
        //goes away after battle
        //stacks with/cancels out base strength/resistance
        //meaning if you're strong against ice you take 50% damage and if weak agains ice you take normal damage
    }

    #endregion

    #endregion
}
