﻿using System;

public class Stuff
{
    // I put the things in here i thought would be useful for the game but didn't know where else to put it
    //each thing should probably be in it's own class though...
    public Dictionary<int, int> levels = new Dictionary<int, int>(); //level chart: Level, total XP
    public Dictionary<int, Monster> monsterDict = new Dictionary<int, Monster>();// list of monsters

    public Stuff()
	{
        FillLevelDictionary();
	}

    private void FillLevelDictionary()
    {

        int i = 1;
        while (i <= 100)
        {
            levels.add(i, (i+1) ^ 3 + 50);
            ++i;

        }
    }

}
